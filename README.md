# Covid-19 Electronic Stethoscope

Covid-19 is deadly through its complication of SARS/pneumonia. In a pandemic, early access to doctors may be limited. This project creates a cheap mass instrument and software for early monitoring of SARS/pneumonia in conditions of self-isolation.